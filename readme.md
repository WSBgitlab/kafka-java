## Interação com kafka via comandos

Lembrando que os binário serão executados diretamente no kafka

Criando um tópico no kafka

```bash
    /bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic NAME_TOPIC_DEFAULT 
```

Listar o tópico criado

```bash
    /bin/kafka-topics.sh --list --bootstrap-server localhost:9092 
```

Enviar mensagens no tópico

```bash
    /bin/kafka-console-producer.sh --broker-list localhost:9092 --topic NAME_TOPIC_DEFAULT
    > pedido1,232
    > pedido2,245
```

Listar as mensagens enviadas

```bash
    /bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic NAME_TOPIC_DEFAULT --from-beginning
```

Aumentando o número de partições em um tópico
Mudando de 1 (valor padrão dentro do arquivo de configuração) para 3 partições 

```bash
    /bin/kafka-topics.sh --alter --zookeeper localhost:2181 --topic NAME_TOPIC_DEFAULT --partitions 3
```


{"evento":[{ "AlertGroup": "Zabbix", "Subject": "Problem: cipnpcprdbidb01: Disk I/O is overloaded on cipnpcprdbidb01.", "Timestamp": "2020-02-07 13:33:05", "ItemName": "CPU iowait time", "ItemValue": "31.22 %", "Summary": "Problem: cipnpcprdbidb01: Disk I/O is overloaded on cipnpcprdbidb01.", "Node": "cipnpcprdbidb01", "IP": "172.16.107.123", "Severity": 2, "EventID": "1065055", "TriggerID": "20957", "Status": "PROBLEM"  }]}

package org.acme.Event;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.smallrye.reactive.messaging.kafka.Record;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.UUID;

@ApplicationScoped
public class EventProducer {

    @Inject @Channel("event-out")
    Emitter<Record<String,String>> emmiter;

    public void enviarEventoKafka(Event evento) throws JsonProcessingException {
        // instancia do mapper.
        ObjectMapper mapper = new ObjectMapper();

        /**
         * Objeto json será o principal, dentro dele incluiremos nossos campos (array,objetos)
         */
        ObjectNode eventoJson = mapper.createObjectNode();
        /**
         * Criando o objeto que será alimentado com o campos na requisição.
         * {}
         * */
        ArrayNode eventArray = mapper.createArrayNode();

        /**
         *
         * Criando um objeto e incluindo o array de eventos.
         * [{}]
         * */
        ObjectNode camposEvento = eventArray.addObject();

        /**
         * Atribuindo ao objeto dentro do array os campos.
         * [{ "AlertGroup" : "conteudo" ...}]
         * */
        camposEvento.put("AlertGroup",evento.AlertGroup);
        camposEvento.put("Subject",evento.Subject);
        camposEvento.put("Problem",evento.Problem);
        camposEvento.put("Timestamp",evento.Timestamp);
        camposEvento.put("ItemName",evento.ItemName);
        camposEvento.put("ItemValue",evento.ItemValue);
        camposEvento.put("Summary",evento.Summary);
        camposEvento.put("Node",evento.Node);
        camposEvento.put("Ip",evento.Ip);
        camposEvento.put("Severity",evento.Severity);
        camposEvento.put("EventId",evento.EventId);
        camposEvento.put("TriggerId",evento.TriggerId);
        camposEvento.put("Status",evento.Status);


        /**
         * Com objeto construido invocamos no objeto principal um campo chamado evento
         * e atribuimos a ele nosso eventArray [{ "AlertGroup" : "conteudo" }]
         * com todos os dados preenchidos atribuimos
         * */

        eventoJson.set("evento", eventArray);

        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(eventoJson);

        System.out.println(json);
        emmiter.send(Record.of(UUID.randomUUID().toString(), json));
    }
}
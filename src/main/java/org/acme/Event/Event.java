package org.acme.Event;

public class Event {
    public String AlertGroup;
    public String Subject;
    public String Problem;
    public String Timestamp;
    public String ItemName;
    public String ItemValue;
    public String Summary;
    public String Node;
    public String Ip;
    public String Severity;
    public String EventId;
    public String TriggerId;
    public String Status;
    

}

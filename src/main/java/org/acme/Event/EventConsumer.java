package org.acme.Event;

import io.smallrye.reactive.messaging.kafka.Record;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;


@ApplicationScoped
public class EventConsumer {
    private final Logger logger = Logger.getLogger(EventConsumer.class);

    @Incoming("event-in")
    public void receive(Record<String, String> record) {
        logger.infof("Got a evento: %s - %s", record.key(), record.value());
    }
}
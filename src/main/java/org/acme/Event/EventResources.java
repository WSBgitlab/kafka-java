package org.acme.Event;

import com.fasterxml.jackson.core.JsonProcessingException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/event")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EventResources {
    @Inject
    EventProducer producer;

    @POST
    public Response send(Event evento) throws JsonProcessingException {
        producer.enviarEventoKafka(evento);
        return Response.ok(evento).build();
    }
}